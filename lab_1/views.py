from django.shortcuts import render
from datetime import datetime, date
# Enter your name here
mhs_name = 'Tesalonika Julia Petronella Sagala' # TODO Implement this
curr_year = int(datetime.now().strftime("%Y"))
birth_date = date(2000, 7, 13) #TODO Implement this, format (Year, Month, Date)
npm = 1806191326 # TODO Implement this
college = 'Universitas Indonesia'
hobby = 'watching movie'
note = 'majoring in Information System'
year = 'Quanta 2018'
# Create your views here.
def index(request):
    response = {'name': mhs_name, 'age': calculate_age(birth_date.year), 'npm': npm, 'college' : college, 'hobby' : hobby, 'note' : note, 'year' : year}
    return render(request, 'index_lab1.html', response)

def calculate_age(birth_year):
    return curr_year - birth_year if birth_year <= curr_year else 0
